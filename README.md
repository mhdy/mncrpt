# mncrpt

mncrpt is a POSIX compliant script for ciphering files using AES-256-CBC. 

## Installation

Install OpenSSL and give the mncrpt.sh execution permission `chmod +x mncrpt.sh`.

## Usage

```
usage: mncrpt.sh -e -i INPUT_FILE [-o OUTPUT_FILE [-f]] [-r]
       mncrpt.sh -d -i INPUT_FILE [-o OUTPUT_FILE [-f]] [-r]
       mncrpt.sh -h
  -e               Encryption mode
  -d               Decryption mode
  -i INPUT_FILE    Specify input file
  -o OUTPUT_FILE   Specify output file
  -f               Force overwrite if OUTPUT_FILE exists
  -r               Shred input file (use with precaution)
  -h               Display this help and exit
```

mncrpt encrypts the file name as well. When using the decryption mode without 
-o option, mncrpt restores the original file with its original name.

## License

The MIT License.
