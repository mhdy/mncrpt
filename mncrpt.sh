#!/bin/sh

# Copyright (C) 2019 Mehdi.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions: 

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.  

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.  

PROGRAM_NAME="$(basename $0)"

EXIT_SUCCESS=0
EXIT_FAILURE=1

fail() {
  echo "$PROGRAM_NAME: $1" >&2
  exit $EXIT_FAILURE
}

randstr() {
  tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c 10 | xargs
}

BFNB='------- BEGIN FILENAME BLOCK -------'
EFNB='------- END FILENAME BLOCK -------'
BFB='------- BEGIN FILE BLOCK -------'
EFB='------- END FILE BLOCK -------'

# write_output <output> <password_hash> <enc_filename> <enc_file>
write_output() {
cat << EOF > "$1"
ncrpt
$BFNB
$3
$EFNB
$BFB
$4
$EFB
$2
EOF
openssl dgst -sha256 "$1" | cut -d ' ' -f 2 >> "$1"
}

usage() {
cat << EOF
ncrpt - openssl wrapper for encryption and decryption using AES-256

usage: $PROGRAM_NAME -e -i INPUT [-o OUTPUT [-f]] [-r]
       $PROGRAM_NAME -d -i INPUT [-o OUTPUT [-f]] [-r]
       $PROGRAM_NAME -h

  -e             encryption mode
  -d             decryption mode
  -i INPUT       input file
  -o OUTPUT      output file
  -f             force overwriting if output exists
  -r             remove input (use with precaution)
  -h             display this help and exit
EOF
}

if [ $# -eq 0 ]
then
  usage
  exit $EXIT_SUCCESS
fi

###
### getopts
###

eflag=0
dflag=0
iflag=0
oflag=0
fflag=0
rflag=0

iarg=""
oarg=""

while getopts ":edi:o:frh" opt
do
  case $opt in
    e) eflag=1 ;;
    d) dflag=1 ;;
    i) iflag=1; iarg="$OPTARG" ;;
    o) oflag=1; oarg="$OPTARG" ;;
    f) fflag=1 ;;
    r) rflag=1 ;;
    h) usage; exit $EXIT_SUCCESS ;;
    \?) fail "invalid options" ;;
    :) fail "option -$OPTARG requires an argument" ;;
  esac
done

# checking option consistency
[ $eflag -eq $dflag ] && fail "invalid options"
[ $oflag -eq 0 -a $fflag -eq 1 ] && fail "invalid options"

# checking input validity
[ $iflag -eq 0 ] && fail "missing input file"
[ ! -f "$iarg" ] && fail "invalid input file"
if [ $dflag -eq 1 ]
then
  isum="$(head -n -1 "$iarg" | openssl dgst -sha256 | cut -d ' ' -f 2)"
  osum="$(tail -1 "$iarg")"
  [ "$isum" != "$osum" ] && fail "invalid input file"
fi

# checking output validity
if [ $oflag -eq 1 ]
then
  [ "$oarg" = "$iarg" ] && fail "invalid output file"
  if [ -f "$oarg" -a $fflag -eq 0 ]
  then
    echo "$PROGRAM_NAME: \"$oarg\" already exists"
    read -p "overwrite it? (y/N) " overwrite
    if [ "$overwrite" = "y" ]
    then
      fflag=1
    else
      exit $EXIT_SUCCESS
    fi
  fi
fi

###
### main
###

# reading password
stty -echo
read -p "password: " password
echo
if [ $eflag -eq 1 ]
then
  read -p "confirm password: " confirmation
  echo
fi
stty echo

# setting input
input="$iarg"

if [ $eflag -eq 1 ]
then
  # checking password
  [ "$password" != "$confirmation" ] && fail "invalid password confirmation"
  # setting output
  output="$oarg"
  if [ $oflag -eq 0 ]
  then
    output=$(randstr).ncrpt
    while [ -f "$oarg" ]
    do
      ouput=$(randstr).ncrpt
    done
  fi
  # processing
  ph="$(echo "$password" | openssl dgst -sha256 | cut -d ' ' -f 2)"
  efn="$(basename "$input" | openssl enc -e -aes-256-cbc -pbkdf2 -iter 100 -a -salt -pass pass:"$password")"
  ef=$(openssl enc -e -aes-256-cbc -pbkdf2 -iter 100 -a -salt -pass pass:"$password" -in "$input")
  write_output "$output" "$ph" "$efn" "$ef"
fi

if [ $dflag -eq 1 ]
then
  # checking password
  password_hash="$(echo "$password" | openssl dgst -sha256 | cut -d ' ' -f 2)"
  stored_hash="$(grep -E -m 1 '[0-9a-f]{55}' "$iarg")"
  [ "$password_hash" != "$stored_hash" ] && fail "invalid password"
  # setting output
  output="$oarg"
  if [ $oflag -eq 0 ]
  then
    output="`sed -n "/$BFNB/,/$EFNB/p" "$input" | \
      sed -e '1d' -e '$d' | \
      openssl enc -d -aes-256-cbc -pbkdf2 -iter 100 -a -pass pass:"$password" \
      2> /dev/null`"
  fi
  # processing
  sed -n "/$BFB/,/$EFB/p" "$input"  | \
    sed -e '1d' -e '$d' | \
    openssl enc -d -aes-256-cbc -pbkdf2 -iter 100 -a -pass pass:"$password" -out "$output" \
    2> /dev/null
fi

[ $rflag -eq 1 ] && shred -fuz "$input"

exit $EXIT_SUCCESS
